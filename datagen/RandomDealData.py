import time
import numpy, random
from datetime import datetime, timedelta
import json
from Instrument import Instrument
from flask import Flask, Response
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

instruments = ("Astronomica", "Borealis", "Celestial", "Deuteronic", "Eclipse",
			"Floral", "Galactia", "Heliosphere", "Interstella", "Jupiter", "Koronis", "Lunatic")
counterparties = ("Lewis", "Selvyn", "Richard", "Lina", "John", "Nidia")
NUMBER_OF_RANDOM_DEALS = 20
TIME_PERIOD_MILLIS = 3600000
EPOCH = datetime.now() - timedelta(days = 1)

class RandomDealData:
    
    def __init__(self):
        self.instrumentList = []
    def createInstrumentList(self):
        f = open('initialRandomValues.txt', 'r')
        instrumentId = 1
        for instrumentName in instruments:
            hashedValue = int(f.readline())
            isNegative = hashedValue < 0
            basePrice = (abs(hashedValue) % 10000) + 90.0
            drift = ((abs(hashedValue) % 5) * basePrice) / 1000.0
            drift = 0 - drift if isNegative else drift
            variance = (abs(hashedValue) % 1000) / 100.0
            variance = 0 - variance if isNegative else variance
            instrument = Instrument(instrumentId, instrumentName, basePrice, basePrice, drift, variance)
            self.instrumentList.append(instrument)
            instrumentId += 1
        return self.instrumentList


    def createRandomData( self, instrumentList ):
        time.sleep(random.uniform(1,30)/100)
        instrument = instrumentList[numpy.random.randint(0,len(instrumentList))]
        cpty = counterparties[numpy.random.randint(0,len(counterparties))]
        type = 'B' if numpy.random.choice([True, False]) else 'S'
        quantity = int( numpy.power(1001, numpy.random.random()))
        dealTime = datetime.now() - timedelta(days = 1)
        deal = {
            'instrumentId' : instrument.ID,
            'instrumentName' : instrument.name,
            'cpty' : cpty,
            'price' : instrument.calculateNextPrice(type),
            'type' : type,
            'quantity' : quantity,
            'time' : dealTime.strftime("%d-%b-%Y (%H:%M:%S.%f)"),
            }
            
        return(json.dumps(deal)) 

@app.route("/gendata")
def main():
    random_deal_data = RandomDealData()
    instrument_list = random_deal_data.createInstrumentList()
    def event_stream():
        while True:
            time.sleep(1)
            yield random_deal_data.createRandomData(instrument_list) + "\n\n"
            

    return Response(event_stream(), status=200, mimetype="text/event-stream")

def bootapp():
    app.run(port=8080, threaded=True, host=("0.0.0.0"))
