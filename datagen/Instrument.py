import numpy

class Instrument:

    def __init__(self, ID, name, price, startingPrice, drift, variance):
        self.ID = ID
        self.price = price
        self.name = name
        self.startingPrice = startingPrice
        self.drift = drift
        self.variance = variance

    def calculateNextPrice(self, direction):
        newPriceStarter = self.price + numpy.random.normal(0,1)*self.variance + self.drift
        newPrice = newPriceStarter if (newPriceStarter > 0) else 0.0
        if self.price < self.startingPrice*0.4:
            self.drift =( -0.7 * self.drift )
        self.price = newPrice*1.01 if direction=='B' else newPrice*0.99
        return self.price
       
