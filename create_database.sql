DROP DATABASE IF EXISTS deals;
CREATE DATABASE deals;
USE deals;

CREATE TABLE Instruments (
	instrument_id int NOT NULL AUTO_INCREMENT,
    instrument_name varchar(30),
    PRIMARY KEY (instrument_id)
    );

CREATE TABLE Counterparties (
	counterparty_id int NOT NULL AUTO_INCREMENT,
    counterparty_name varchar(30),
    PRIMARY KEY (counterparty_id)
    );

CREATE TABLE Deals (
	deal_id int NOT NULL AUTO_INCREMENT,
    instrument_id int NOT NULL,
    counterparty_id int NOT NULL,
    price real NOT NULL,
    buy_sell varchar(4),
    quantity int NOT NULL,
    deal_time datetime,
    PRIMARY KEY (deal_id),
	FOREIGN KEY (counterparty_id) REFERENCES Counterparties(counterparty_id),
	FOREIGN KEY (instrument_id) REFERENCES Instruments(instrument_id)
    );

CREATE TABLE Accounts (
	account_id int NOT NULL AUTO_INCREMENT,
    username varchar(30),
    user_password varchar(30),
    user_type varchar(30),
    PRIMARY KEY (account_id)
    );