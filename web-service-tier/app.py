from flask import Flask, Response, send_from_directory
from flask_restful import Resource, Api, reqparse
from flask_cors import CORS
import requests
import json 
import time
import csv
import os


DAO_LINK = "http://controller-casestudygroup23.apps.dbgrads-6eec.openshiftworkshop.com/"

app = Flask(__name__)

CORS(app, supports_credentials =True)

@app.route("/stream")
def stream():
    link = DAO_LINK+"stream"
    r = requests.get(link, stream=True)

    def event_stream():
        for my_json in r.iter_lines():
            
            if my_json:
                yield 'data: {}\n\n'.format(my_json.decode("ascii"))
    return Response(event_stream(), status=200, mimetype="text/event-stream")  

@app.route("/test")
def hello():
    return "Hello World!"

@app.route("/time")
def currenttime_for_web():
    def eventStream():
        while True:
            time.sleep(1)
            yield 'data: {}\n\n'.format(currenttime())
    return Response(eventStream(), status = 200, mimetype="text/event-stream")

def currenttime():
    time.sleep(0)
    my_time = time.ctime(time.time())
    return my_time

@app.route("/login", methods=["POST"])
def login():     
    parser = reqparse.RequestParser()
    parser.add_argument('username', help = 'This field cannot be blank', required = True)
    parser.add_argument('password', help = 'This field cannot be blank', required = True)
    data = parser.parse_args()
    print(data)
    ''' connect to dao and request'''
    link = DAO_LINK + 'login'
    r = requests.post(link, data)
    for my_json in r.iter_lines():
        if my_json:
            response_line = json.loads(my_json)
            return Response(json.dumps(dict(state=response_line['state'])),  status = 200, mimetype="application/json")
    return Response(json.dumps(dict(state='Server is not responding')),  status = 500, mimetype="application/json")


'''
You can also ask for getting the data as a CSV
'''
@app.route("/getalldeals", methods=["POST"])
def getalldeals(): 
    parser = reqparse.RequestParser()
    parser.add_argument('option', help = 'This field cannot be blank', required = True)
    data = parser.parse_args()
    link = DAO_LINK+"getalldeals"
    r = requests.get(link, stream=True)
    if data.option == "csv":
        with open('./temp.csv', mode='w+') as file:
            csvwriter = csv.writer(file)
            count = 0
            for emp in r.json():
                if count == 0:
                    header = emp.keys()
                    csvwriter.writerow(header)
                    count += 1
                csvwriter.writerow(emp.values())
        file.close()
        dir_path = os.getcwd()
        return send_from_directory(dir_path, 'temp.csv', as_attachment=True) 
    else:
        return Response(r, status=200, mimetype="application/json") 

app.route("/getallaverages", methods=['GET'])
def instruments_average_price():
    link = DAO_LINK+"getallaverages"
    r = requests.get(link)
    return Response(r, status=200, mimetype="application/json")  

@app.route("/endingposition", methods=['GET'])
def ending_position():
    link = DAO_LINK+"endingposition"
    r = requests.get(link)
    return Response(r, status=200, mimetype="application/json")  

@app.route("/getalldeals", methods = ["GET"])
def retrieve_all_deals():
    link = DAO_LINK+"getalldeals"
    r = requests.get(link)
    return Response(r, status=200, mimetype="application/json")  

@app.route("/pricevstime",methods = ["GET"])
def price_time_graph_of_instrument():
    link = DAO_LINK+"pricevstime"
    r = requests.get(link)
    return Response(r, status=200, mimetype="application/json")  

@app.route("/effectiveprofit",methods = ["GET"])
def effectiveprofit():
    link = DAO_LINK+"effectiveprofit"
    r = requests.get(link)
    return Response(r, status=200, mimetype="application/json")  

@app.route("/realisedprofit",methods = ["GET"])
def realisedprofit():
    link = DAO_LINK+"realisedprofit"
    r = requests.get(link)
    return Response(r, status=200, mimetype="application/json")  

def bootapp():
    app.run(port=8080, debug=True, threaded=True, host=("0.0.0.0"))

if __name__ == "__main__":
    bootapp()
