import React, {Component} from 'react';
import {Bar, Line, Pie} from 'react-chartjs-2';
import ReactDOM from "react-dom";

class Chart extends Component{
  

componentWillMount(){
    this.getChartData();
}

getChartData(){
// Ajax calls here
    this.setState({
        chartData:{
        labels: ['Boston', 'Worcester', 'Springfield', 'Lowell', 'Cambridge', 'New Bedford'],
        datasets:[
            {
            label:'Population',
            data:[
                617594,
                181045,
                153060,
                106519,
                105162,
                95072
            ],
            backgroundColor:[
                'rgba(255, 99, 132, 0.6)',
                'rgba(54, 162, 235, 0.6)',
                'rgba(255, 206, 86, 0.6)',
                'rgba(75, 192, 192, 0.6)',
                'rgba(153, 102, 255, 0.6)',
                'rgba(255, 159, 64, 0.6)',
                'rgba(255, 99, 132, 0.6)'
            ]
            }
        ]
        }
    });
    const node = ReactDOM.findDOMNode(this);
    var child
    // Get child nodes
    if (node instanceof HTMLElement) {
        console.log('query sel')
        child = node.querySelector('.Chart');
    }

    this.eventSource.onmessage = (m) => { 
        //var z = document.createElement('p'); // is a node
        //z.innerHTML = m.data;
        
        //document.body.appendChild(z);
        console.log(m.data)
        var obj = JSON.parse(m.data)
        console.log(obj["time"])
        
        this.state.chartData["datasets"][0]["data"].push(1)
        this.state.chartData["datasets"][0]["backgroundColor"].push('rgba(255, 99, 132, 0.6)')
        this.state.chartData["labels"].push('example')
        //child.update()
    } 
}

  constructor(props){
    super(props);

    this.eventSource = new EventSource("http://localhost:8080/stream",{withCredentials: true} );
    console.log(this.eventSource.url)
    //this.eventSource.onmessage = (m) => { 
        //var z = document.createElement('p'); // is a node
        //z.innerHTML = m.data;
        
        //document.body.appendChild(z);
        //console.log(m.data)
    //}

    this.state = {
      chartData:props.chartData
    }
  }

  static defaultProps = {
    displayTitle:true,
    displayLegend: true,
    legendPosition:'right',
    location:'City'
  }

  render(){
    return (
      <div className="chart">
        <Bar
          data={this.state.chartData}
          options={{
            title:{
              display:this.props.displayTitle,
              text:'Largest Cities In '+this.props.location,
              fontSize:25
            },
            legend:{
              display:this.props.displayLegend,
              position:this.props.legendPosition
            }
          }}
        />

        <Line
          data={this.state.chartData}
          options={{
            title:{
              display:this.props.displayTitle,
              text:'Largest Cities In '+this.props.location,
              fontSize:25
            },
            legend:{
              display:this.props.displayLegend,
              position:this.props.legendPosition
            }
          }}
        />

        <Pie
          data={this.state.chartData}
          options={{
            title:{
              display:this.props.displayTitle,
              text:'Largest Cities In '+this.props.location,
              fontSize:25
            },
            legend:{
              display:this.props.displayLegend,
              position:this.props.legendPosition
            }
          }}
        />
      </div>
    )
  }
}

export default Chart;