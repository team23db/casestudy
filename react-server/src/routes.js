import React from 'react';

const Dashboard = React.lazy(() => import('./views/Dashboard'));
const Livestream = React.lazy(() => import('./views/livestream'));
const Positions = React.lazy(() => import('./views/positions'));
const Averages = React.lazy(() => import('./views/averages'));
const Charts = React.lazy(() => import('./views/Charts/-charts'));
const Tables = React.lazy(() => import('./views/Tables/ctables'));
const Profit = React.lazy(() => import('./views/profit'));



// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Home' },
  //{ path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/livestream', name: 'Livestream', component: Livestream },
  { path: '/positions', name: 'Ending Positions', component: Positions },
  { path: '/averages', name: 'Price Averages', component: Averages },
  { path: '/charts', name: 'Charts', component: Charts },
  { path: '/tables', name: 'Tables', component: Tables },
  { path: '/profit', name: 'Profit', component: Profit },

];

export default routes;
