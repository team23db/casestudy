export default {
  items: [
    
    {
      name: 'Live Stream',
      url: '/livestream',
      icon: 'fa-spin fa fa-circle-o-notch',
    },
    {
      name: 'Averages',
      url: '/averages',
      icon: 'fa fa-calculator',
    },

    {
      name: 'History',
      url: '/base',
      icon: 'fa fa-history fa-lg mt-2',
      children: [
        {
          name: 'Charts',
          //
          url: '/charts',
          icon: 'fa fa-line-chart',
        },
        {
          name: 'Tables',
          //url: '/base/tables',
          url: '/tables',
          icon: 'fa fa-table fa-lg',
        },          
               
        ],
    },
    {
      name: 'Reports',
      url: '/reports',
      icon: 'fa fa-file-text',
      children: [
        {
          name: 'Ending Positions',
          //url: '/buttons/ending_positions',
          url: '/positions',
          icon: 'cui-sort-descending',
        },
        {
          name: 'Realised\\ Effective Profit',
          //url: '/buttons/realised-profit',
          url: '/profit',
          icon: 'fa fa-money',
        },
        // {
        //   name: ' Effective Profit',
        //   //url: '/buttons/ effective profit ',
        //   url: '/reports/ effective profit ',
        //   icon: 'fa fa-briefcase ',
        // },        
      ],
    },      
     
      
  ],
};
