import React, { Component } from 'react';
import { Button, Card, CardTitle,CardText, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, Alert } from 'reactstrap';
import axios from 'axios';
import { Redirect} from "react-router-dom";

let web_service_tier_address ='http://127.0.0.1:6003/';

export default class Profit extends Component {
  constructor(props) {
      super(props);
      this.handleLoading();
      this.state = {
        realisedProfit: 0,
        endingposition: 0,
        effectiveprofit:0
      };
  }

  handleLoading() {

    axios.get(web_service_tier_address+"realisedprofit")
        .then(response =>
            this.setState({realisedProfit:response.data[0]['realisedprofit']})
        )
        .catch((error) => {
            // handle your errors here
            console.error(error)
        });
      axios.get(web_service_tier_address+"effectiveprofit")
        .then(response =>
            this.setState({effectiveprofit:response.data[0]['effectiveprofit']})
        )
        .catch((error) => {
            // handle your errors here
            console.error(error)
        });
      
        axios.get(web_service_tier_address+"endingposition")
        .then(response =>
            this.setState({endingposition:response.data[0]['endingposition']})
        )
        .catch((error) => {
            // handle your errors here
            console.error(error)
        });
    
    }


  render() {
      return (
      <div className="Profit">
            
            <Row>
            <Col sm="6">
              <Card body>
                <CardTitle>Realised Profit</CardTitle>
                <CardText>{this.state.realisedProfit}</CardText>
              </Card>
            </Col>
            <Col sm="6">
              <Card body>
                <CardTitle>Effective Profit</CardTitle>
                <CardText>{this.state.effectiveprofit}</CardText>
              </Card>
            </Col>
            <Col sm="6">
              <Card body>
                <CardTitle>Ending Position</CardTitle>
                <CardText>{this.state.endingposition}</CardText>
              </Card>
            </Col>
            </Row>
            
      </div>
    );
  }
}