import Charts from './Charts';
import Dashboard from './Dashboard';

import { Login } from './Pages';
import Positions from './positions';
import Tables from './Tables';
import Averages from './averages/averages';
import Profit from './profit';
import Livestream from './livestream';




export {
  //Badges,
  //Typography,
  //Colors,
  //CoreUIIcons,
  //Page404,
  //Page500,
  //Register,
  Login,
  //Modals,
  //Alerts,
  //Flags,
  //SimpleLineIcons,
  //FontAwesome,
  //ButtonDropdowns,
  //ButtonGroups,
  //BrandButtons,
  //Buttons,
  //Tooltips,
  //Tabs,
  //Tables,
  //Charts,
  Dashboard,
  //Widgets,
  //Jumbotrons,
  //Switches,
  //ProgressBar,
  //Popovers,
  //Navs,
  //Navbars,
  //ListGroups,
  //Forms,
  //Dropdowns,
  //Collapses,
  //Carousels,
  //Cards,
  //Breadcrumbs,
  //Paginations,
  Charts,
  Livestream,
  Positions,
  Profit,
  Tables,
  Averages,
};

