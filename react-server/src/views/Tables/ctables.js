import React, { Component } from 'react';
import { Table, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, Alert } from 'reactstrap';
import axios from 'axios';
import { Redirect} from "react-router-dom";

let web_service_tier_address ='http://127.0.0.1:6003/';

export default class Tables extends Component {
  constructor(props) {
      super(props);

      this.state = {
        data: []
      };
  }

  getDataRow(){
    axios.post(web_service_tier_address+'getalldeals',{
        option:'json'
    }).then(res => {
        this.setState({data:res.data})
    })

    return this.state.data.map( (row,index)=>
      {
       return <tr>
        <th key={index}>{index}</th>
        <td>{row['instrument_name']}</td>
        <td>{row['counterparty_name']}</td>
        <td>{row['price']}</td>
        <td>{row['buy_sell']}</td>
        <td>{row['quantity']}</td>
        <td>{row['deal_time']}</td>

      </tr>
      }

    )
  }
 

  render() {
      return (
      <div className="Tables">
             <Table dark >
          <thead>
            <tr>
              <th>#</th>
              <th>Instrument Name</th>
              <th>Counter party</th>
              <th>Price</th>
              <th>Buy/Sell</th>
              <th>Quantity</th>
              <th>Deal time</th>
            </tr>
          </thead>
          <tbody>
            {this.getDataRow()}
          </tbody>
        </Table>
            
      </div>
    );
  }
};