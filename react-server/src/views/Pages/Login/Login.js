import React, { Component } from 'react';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, Alert } from 'reactstrap';
import axios from 'axios';
import { Redirect} from "react-router-dom";

let web_service_tier_address ='http://127.0.0.1:6003/';

export default class Login extends Component {
  constructor(props) {
      super(props);

      this.state = {
          username: "",
          password: "",
          redirect: false,
          loginError: false,
      };
  }

  validateForm() {
      return this.state.username.length > 0 && this.state.password.length > 0;
  }

  handleChange = event => {
      this.setState({
          [event.target.id]: event.target.value
      });
  }

  handleSubmit = event => {
      event.preventDefault();

      console.log(this.state.username)

      axios.post(web_service_tier_address+'login',{
          username: this.state.username,
          password: this.state.password,
      }).then(res => {
          if(res.data.state === "correct"){
              this.setState({ redirect: true });
          } else {
            this.setState({loginError:true});
          }
      })


  }


  render() {
    const { redirect } = this.state;
    const { loginError} = this.state;
    return (
      <div className="Login">
            {redirect && <Redirect to='/tables'  />}
            <div className="app flex-row align-items-center">
              <Container>
                <Row className="justify-content-center">
                  <Col md="8">
                    <CardGroup>
                      <Card className="p-4">
                        <CardBody>
                          <Form onSubmit={this.handleSubmit}>
                            <h1>Login</h1>
                            <p className="text-muted">Sign In to your account</p>
                            <InputGroup className="mb-3">
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                  <i className="icon-user"></i>
                                </InputGroupText>
                              </InputGroupAddon>
                              <Input  id="username" autoFocus
                                type="text"
                                placeholder="Enter Trader Account"
                                value={this.state.username}
                                onChange={this.handleChange} />
                            </InputGroup>
                            <InputGroup  className="mb-4">
                              <InputGroupAddon addonType="prepend">
                                <InputGroupText>
                                  <i className="icon-lock"></i>
                                </InputGroupText>
                              </InputGroupAddon>
                              <Input id="password" placeholder="Password"
                                value={this.state.password}
                                onChange={this.handleChange}
                                type="password" />
                            </InputGroup>
                            {loginError && 
                              <Row>
                                  <Alert color="danger" className="px-4">
                                  Wrong username or password
                                  </Alert>
                            </Row>}  
                            <Row> 
                                <Button color="primary" className="px-4" block
                                disabled={!this.validateForm()}
                                type="submit">Login</Button>
                            </Row>
                          </Form>
                        </CardBody>
                      </Card>
                    </CardGroup>
                  </Col>
                </Row>
              </Container>
            </div>
      </div>
    );
  }
};