import React, { Component } from 'react';
import axios from 'axios';
import { Redirect} from "react-router-dom";
import {Bar} from 'react-chartjs-2';



// Graph to display average prices of each instrument
let initialState = {
  labels: ["Astronomica", "Borealis", "Celestial", "Deuteronic", "Eclipse",
  "Floral", "Galactia", "Heliosphere", "Interstella", "Jupiter", "Koronis", "Lunatic"],
  datasets: [
    {
        label: 'Average prices of each instrument',
        backgroundColor: 'rgba(255,99,132,0.2)',
        borderColor: 'rgba(255,99,132,1)',
        borderWidth: 1,
        hoverBackgroundColor: 'rgba(255,99,132,0.4)',
        hoverBorderColor: 'rgba(255,99,132,1)',
        data:[]
    }
  ]
};


let web_service_tier_address ='http://127.0.0.1:6003/';

export default class Login extends Component {
  constructor(props) {
      super(props);

      this.state = {
   
      };
  }

  componentWillMount(){
        
    fetch("http://localhost:6003/getallaverages")
        .then(response => response.json())
        .then((jsonData) => {
            console.log(jsonData[0])
            for (var i = 0; i < jsonData.length; i++) {
                //var obj = JSON.parse(jsonData[i])
                //console.log(jsonData[i]["AVG(price)"])
                initialState["datasets"][0]["data"][i] = (jsonData[i]["AVG(price)"])
            }
            this.setState(initialState);
        })
        .catch((error) => {
            // handle your errors here
            console.error(error)
        })
    
    }


  render() {
      return (
      <div className="Averages">
            
            <Bar data={this.state} />

      </div>
    );
  }
};