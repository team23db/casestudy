import React, { Component } from 'react';
//import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, Alert } from 'reactstrap';
import axios from 'axios';
import { Redirect} from "react-router-dom";
import {Line} from 'react-chartjs-2';

let web_service_tier_address ='http://127.0.0.1:6003/';

const initialState = {
  labels: [],
  datasets: [
    {
      label: 'Astronomica',
      fill: false,
      lineTension: 0.1,
      backgroundColor: 'rgba(0,0,0,0.4)',
      borderColor: 'rgba(153,76,0,1)',
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: 'rgba(75,192,192,1)',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: 'rgba(75,192,192,1)',
      pointHoverBorderColor: 'rgba(220,220,220,1)',
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: []
    },
    {
      label: "Borealis",
      fill: false,
      lineTension: 0.1,
      backgroundColor: 'rgba(75,192,192,0.4)',
      borderColor: 'rgba(255,255,51,1)',
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: 'rgba(75,192,192,1)',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: 'rgba(75,192,192,1)',
      pointHoverBorderColor: 'rgba(220,220,220,1)',
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: []
    },
    {
      label: "Celestial",
      fill: false,
      lineTension: 0.1,
      backgroundColor: 'rgba(75,192,192,0.4)',
      borderColor: 'rgba(51,255,51,1)',
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: 'rgba(75,192,192,1)',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: 'rgba(75,192,192,1)',
      pointHoverBorderColor: 'rgba(220,220,220,1)',
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: []
    },
    {
      label: 'Deuteronic',
      fill: false,
      lineTension: 0.1,
      backgroundColor: 'rgba(75,192,192,0.4)',
      borderColor: 'rgba(51,255,255,1)',
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: 'rgba(75,192,192,1)',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: 'rgba(75,192,192,1)',
      pointHoverBorderColor: 'rgba(220,220,220,1)',
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: []
    },
    {
      label: "Eclipse",
      fill: false,
      lineTension: 0.1,
      backgroundColor: 'rgba(75,192,192,0.4)',
      borderColor: 'rgba(51,51,255,1)',
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: 'rgba(75,192,192,1)',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: 'rgba(75,192,192,1)',
      pointHoverBorderColor: 'rgba(220,220,220,1)',
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: []
    },
    {
      label: "Floral",
      fill: false,
      lineTension: 0.1,
      backgroundColor: 'rgba(75,192,192,0.4)',
      borderColor: 'rgba(255,51,255,1)',
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: 'rgba(75,192,192,1)',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: 'rgba(75,192,192,1)',
      pointHoverBorderColor: 'rgba(220,220,220,1)',
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: []
    },
    {
      label: 'Galactia',
      fill: false,
      lineTension: 0.1,
      backgroundColor: 'rgba(75,192,192,0.4)',
      borderColor: 'rgba(255,51,153,1)',
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: 'rgba(75,192,192,1)',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: 'rgba(75,192,192,1)',
      pointHoverBorderColor: 'rgba(220,220,220,1)',
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: []
    },
    {
      label: "Heliosphere",
      fill: false,
      lineTension: 0.1,
      backgroundColor: 'rgba(75,192,192,0.4)',
      borderColor: 'rgba(102,0,51,1)',
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: 'rgba(75,192,192,1)',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: 'rgba(75,192,192,1)',
      pointHoverBorderColor: 'rgba(220,220,220,1)',
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: []
    },
    {
      label: "Interstella",
      fill: false,
      lineTension: 0.1,
      backgroundColor: 'rgba(75,192,192,0.4)',
      borderColor: 'rgba(51,0,102,1)',
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: 'rgba(75,192,192,1)',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: 'rgba(75,192,192,1)',
      pointHoverBorderColor: 'rgba(220,220,220,1)',
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: []
    },
    {
      label: 'Jupiter',
      fill: false,
      lineTension: 0.1,
      backgroundColor: 'rgba(75,192,192,0.4)',
      borderColor: 'rgba(0,102,102,1)',
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: 'rgba(75,192,192,1)',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: 'rgba(75,192,192,1)',
      pointHoverBorderColor: 'rgba(220,220,220,1)',
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: []
    },
    {
      label: "Koronis",
      fill: false,
      lineTension: 0.1,
      backgroundColor: 'rgba(75,192,192,0.4)',
      borderColor: 'rgba(255,51,153,1)',
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: 'rgba(75,192,192,1)',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: 'rgba(75,192,192,1)',
      pointHoverBorderColor: 'rgba(220,220,220,1)',
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: []
    },
    {
      label: "Lunatic",
      fill: false,
      lineTension: 0.1,
      backgroundColor: 'rgba(75,192,192,0.4)',
      borderColor: 'rgba(0,0,102,1)',
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: 'rgba(75,192,192,1)',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: 'rgba(75,192,192,1)',
      pointHoverBorderColor: 'rgba(220,220,220,1)',
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: []
    }
  ]
};


export default class Chart extends Component {
  constructor(props) {
      super(props);

      this.state = {
   
      };
  }

  componentWillMount(){
    fetch("http://localhost:6003/pricevstime")
      .then(response => response.json())
      .then((jsonData) => {
        console.log(jsonData[0])
        for (var i = 0; i < jsonData.length; i++) {
          //var obj = JSON.parse(jsonData[i])
          //console.log(jsonData[i]["AVG(price)"])
          if(jsonData[i]["instrument_name"] == "Astronomica") 
          {
            initialState["datasets"][0]["data"].push(jsonData[i]["price"])  
            //initialState["labels"].push(jsonData[i]["deal_time"])
          }
          else if (jsonData[i]["instrument_name"] == "Borealis") 
          {
           initialState["datasets"][1]["data"].push(jsonData[i]["price"])  
           //initialState["labels"].push(jsonData[i]["deal_time"])
          }
          else if (jsonData[i]["instrument_name"] == "Celestial") 
          {
            initialState["datasets"][2]["data"].push(jsonData[i]["price"])  
            //initialState["labels"].push(jsonData[i]["deal_time"])
          }
          else if (jsonData[i]["instrument_name"] == "Deuteronic")
          {
            initialState["datasets"][3]["data"].push(jsonData[i]["price"])
            //initialState["labels"].push(jsonData[i]["deal_time"])
          }
          else if (jsonData[i]["instrument_name"] == "Eclipse") 
          {
           initialState["datasets"][4]["data"].push(jsonData[i]["price"])  
           //initialState["labels"].push(jsonData[i]["deal_time"])
          }
          else if (jsonData[i]["instrument_name"] == "Floral") 
          {
            initialState["datasets"][5]["data"].push(jsonData[i]["price"])  
            initialState["labels"].push(jsonData[i]["deal_time"])
          }
          else if (jsonData[i]["instrument_name"] == "Galactia")
          {
            initialState["datasets"][6]["data"].push(jsonData[i]["price"])
            //initialState["labels"].push(jsonData[i]["deal_time"])
          }
          else if (jsonData[i]["instrument_name"] == "Heliosphere") 
          {
           initialState["datasets"][7]["data"].push(jsonData[i]["price"])  
           //initialState["labels"].push(jsonData[i]["deal_time"])
          }
          else if (jsonData[i]["instrument_name"] == "Interstella") 
          {
            initialState["datasets"][8]["data"].push(jsonData[i]["price"])  
            //initialState["labels"].push(jsonData[i]["deal_time"])
          }
          else if (jsonData[i]["instrument_name"] == "Jupiter")
          {
            initialState["datasets"][9]["data"].push(jsonData[i]["price"])
            //initialState["labels"].push(jsonData[i]["deal_time"])
          }
          else if (jsonData[i]["instrument_name"] == "Koronis") 
          {
           initialState["datasets"][10]["data"].push(jsonData[i]["price"])  
           //initialState["labels"].push(jsonData[i]["deal_time"])
          }
          else if (jsonData[i]["instrument_name"] == "Lunatic") 
          {
            initialState["datasets"][11]["data"].push(jsonData[i]["price"])  
            //initialState["labels"].push(jsonData[i]["deal_time"])
          }
        

        }
        this.setState(initialState);
      })
      .catch((error) => {
        // handle your errors here
        console.error(error)
      })
      
  }
  render() {
      return (
      <div className="Charts">
                <Line data={this.state}/>
  
            
      </div>
    );
  }
};