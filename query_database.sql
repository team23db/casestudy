/* For retrieving all historical data according to specified time period. Insert times from front end into datetime strings.
*/
SELECT Instruments.instrument_name, Counterparties.counterparty_name, Deals.price, Deals.buy_sell, Deals.quantity, Deals.deal_time
FROM Deals
INNER JOIN Instruments ON Deals.instrument_id=Instruments.instrument_id
INNER JOIN Counterparties ON Deals.counterparty_id=Counterparties.counterparty_id
WHERE deal_time >= '2019-08-11 12:07:06' AND deal_time <= '2019-08-11 12:07:08';

/* Most recent price */ 
SELECT instrument_name, price, deal_time FROM Deals INNER JOIN Instruments ON Deals.instrument_id=Instruments.instrument_id 
order by instrument_name, deal_time;

/*  For calculating the average price of each instrument.
*/
SELECT instrument_name, AVG(price) FROM Deals INNER JOIN Instruments ON Deals.instrument_id=Instruments.instrument_id GROUP BY Deals.instrument_id;

/* For calculating realised profit of each counterparty. Returns two values; buy and sell. Subtract buy from sell to calculate net realised profit.
*/
SELECT * from 
(SELECT SUM(total_paid) as total_paid from (SELECT price*quantity AS total_paid, buy_sell FROM Deals 
INNER JOIN Counterparties ON Deals.counterparty_id=Counterparties.counterparty_id
WHERE counterparty_name='Lewis' 
ORDER BY buy_sell) as t1 group by buy_sell) as t2;

/* For returning price and time columns for a particular instrument. Insert instrument name into string.
*/
SELECT price, deal_time FROM Deals INNER JOIN Instruments ON Deals.instrument_id=Instruments.instrument_id WHERE instrument_name='Eclipse';

select sum(t1.price * t1.quantity) as total from (SELECT instrument_id, buy_sell, price, quantity, deal_time
FROM Deals where instrument_id = 1 and buy_sell = 'S'
ORDER BY instrument_id, buy_sell limit 3) as t1;

SELECT price, quantity FROM Deals WHERE instrument_id = 1 AND buy_sell='B' ORDER BY deal_time;

SELECT instrument_id, buy_sell, price, quantity, deal_time
FROM Deals where instrument_id = 1 and buy_sell = 'S'
ORDER BY instrument_id, buy_sell;

SELECT instrument_id, buy_sell, price, quantity FROM Deals WHERE instrument_id = 2 and buy_sell = 'B' ORDER BY instrument_id, buy_sell;
SELECT * from (SELECT instrument_id, buy_sell, price, quantity, deal_time
FROM Deals
ORDER BY instrument_id, buy_sell) as t1 inner join (SELECT instrument_id, buy_sell, price, quantity, deal_time
FROM Deals
ORDER BY instrument_id, buy_sell) as t2 on t1.deal_time = t2.deal_time AND t1.instrument_id >= t2.instrument_id;

SELECT SUM(price*quantity) FROM Deals GROUP BY buy_sell;

SELECT SUM(price*quantity) FROM Deals WHERE buy_sell='S' AND instrument_id=1;