import time
import mysql.connector
from mysql.connector import Error
from datetime import datetime

class db:  
    def __init__(self):
        self.cursor = None
        self.mydb = None

    def login(self, data):
        self.open_db_connection()
        query = "select user_password from Accounts where username = '"+data.username+"'"
        self.cursor.execute(query)
        record = self.cursor.fetchone()
        self.close_db_connection()
        return record

    def insert_deals(self, data):
        instrument_id = data["instrumentId"]
        instrument_name = data["instrumentName"]
        price = data["price"]
        cpty = data["cpty"]
        deal_type = data["type"]
        quantity = data["quantity"]
        time = data["time"][:21]
        datetime_object = datetime.strptime(time, '%d-%b-%Y (%H:%M:%S')
        #counterparty_list = rdd.counterparties
        print(data)
        try:
            self.cursor.execute("SELECT counterparty_name from Counterparties WHERE counterparty_name = %s;", (cpty,))
            counterparty_name = self.cursor.fetchone()
            #print("counterparty", type(counterparty_name))
            if counterparty_name == None:
                #print("Insert into counterparty")
                self.cursor.execute("INSERT INTO Counterparties (counterparty_name) VALUES (%s)", (cpty,))
                self.mydb.commit()
        except Error as e:
            pass
            
        try:
            self.cursor.execute("SELECT instrument_name from Instruments WHERE instrument_name = %s;", (instrument_name,))
            instr_name = self.cursor.fetchone()
           # print("instrname", type(instr_name))
            if instr_name == None:
                #print("Insert into instruments")
                self.cursor.execute("INSERT INTO Instruments (instrument_name) VALUES (%s)", (instrument_name,))
                self.mydb.commit()
        except Error as e:
            pass

        print("Inserting deals into Deals table")
        self.cursor.execute("SELECT counterparty_id from Counterparties WHERE counterparty_name = %s;", (cpty,))
        counterparty_id = self.cursor.fetchone()
        self.cursor.execute("INSERT INTO Deals (instrument_id, counterparty_id, price, buy_sell, quantity, deal_time) VALUES (%s,%s,%s,%s,%s,%s)", 
        (instrument_id, counterparty_id[0], price, deal_type, quantity, datetime_object))
        self.mydb.commit()

    def open_db_connection(self):
        try:
            self.mydb = mysql.connector.connect(
                host="192.168.99.100", # Database link 
                database="deals",
                user="root",
                password="ppp"
            )
            if self.mydb.is_connected():
                db_Info = self.mydb.get_server_info()
                print("Connected to MySQL database... MySQL Server version on ",db_Info)
                self.cursor = self.mydb.cursor(buffered=True)
                self.cursor.execute("select database();")
                record = self.cursor.fetchone()
                print ("Your connected to - ", record)
                print("-------------------------------")
            
        except Error as e :
            print ("Error while connecting to MySQL", e)
    
    def retrieve_all_deals(self, start_date=None, end_date=None):

        if start_date and end_date:
            query = """SELECT Instruments.instrument_name, Counterparties.counterparty_name, Deals.price, 
                    Deals.buy_sell, Deals.quantity, Deals.deal_time FROM Deals INNER JOIN Instruments ON 
                    Deals.instrument_id=Instruments.instrument_id INNER JOIN Counterparties ON 
                    Deals.counterparty_id=Counterparties.counterparty_id WHERE deal_time >= %s AND deal_time <= %s;"""
        else:
            query = """SELECT Instruments.instrument_name, Counterparties.counterparty_name, Deals.price, 
                    Deals.buy_sell, Deals.quantity, Deals.deal_time FROM Deals INNER JOIN Instruments ON 
                    Deals.instrument_id=Instruments.instrument_id INNER JOIN Counterparties ON 
                    Deals.counterparty_id=Counterparties.counterparty_id;"""
        self.open_db_connection()
        if start_date and end_date:
            self.cursor.execute(query, (start_date, end_date))
        else:
            self.cursor.execute(query)
        row_headers=[x[0] for x in self.cursor.description]
        records = self.cursor.fetchall()
        self.close_db_connection()
        return(row_headers, records)

    def realised_profit(self):
        total_cost = 0
        total_revenue = 0
        # Function to return list of instrument ID's
        query = "SELECT DISTINCT(instrument_id) FROM Deals;"
        self.open_db_connection()
        
        self.cursor.execute(query)
        instrument_ids = []
        for row in self.cursor:
            instrument_ids.append(row[0])
        
        # Function to return quantities of buy and sell for particular instrument
        for instrument_id in instrument_ids:
            query = "SELECT buy_sell, SUM(quantity) FROM Deals WHERE instrument_id = %s GROUP BY instrument_id, buy_sell;"
            self.cursor.execute(query, (instrument_id,))
            buy_sell_quantities = self.cursor.fetchall()

            buy_quantity = int(buy_sell_quantities[0][1])
            sell_quantity = int(buy_sell_quantities[1][1])

            if  buy_quantity >= sell_quantity:               
                # get realized revenue from all S
                query = "SELECT SUM(price*quantity) FROM Deals WHERE buy_sell='S' AND instrument_id=%s;"
                self.cursor.execute(query, (instrument_id,))
                instrument_total_revenue = self.cursor.fetchone()[0]
               # print(instrument_total_sells)
                # get realized costs from all B
                query = """SELECT price, quantity 
                    FROM Deals WHERE instrument_id = %s and buy_sell = 'B' ORDER BY instrument_id, buy_sell;"""
                self.cursor.execute(query, (instrument_id,))
                count_quantity = 0 
                instrument_total_cost = 0
                for price, quantity in self.cursor:
                    count_quantity += quantity
                    if count_quantity < sell_quantity:
                        instrument_total_cost += price * quantity
                    elif count_quantity == sell_quantity: 
                        instrument_total_cost += price * quantity
                        break;
                    else:
                        count_quantity -= quantity
                        remaining_quantity = sell_quantity - count_quantity
                        instrument_total_cost += remaining_quantity * price
                        break;
                total_cost += instrument_total_cost
                total_revenue += instrument_total_revenue

            else:
                # get realized costs from all S
                query = "SELECT SUM(price*quantity) FROM Deals WHERE buy_sell='B' AND instrument_id=%s;"
                self.cursor.execute(query, (instrument_id,))
                instrument_total_cost = self.cursor.fetchone()[0]
               # print(instrument_total_sells)

                # get realized costs from all B
                query = """SELECT price, quantity 
                    FROM Deals WHERE instrument_id = %s and buy_sell = 'S' ORDER BY instrument_id, buy_sell;"""
                self.cursor.execute(query, (instrument_id,))
                count_quantity = 0 
                instrument_total_revenue = 0
                for price, quantity in self.cursor:
                    count_quantity += quantity
                    if count_quantity < buy_quantity:
                        instrument_total_revenue += price * quantity
                    elif count_quantity == buy_quantity: 
                        instrument_total_revenue += price * quantity
                        break;
                    else:
                        count_quantity -= quantity
                        remaining_quantity = buy_quantity - count_quantity
                        instrument_total_revenue += remaining_quantity * price
                        break;
                total_cost += instrument_total_cost
                total_revenue += instrument_total_revenue


        return total_revenue-total_cost
    
    def effective_profit(self):
        total_effective_profit = 0
        # Function to return list of instrument ID's
        query = "SELECT DISTINCT(instrument_id) FROM Deals;"
        self.open_db_connection()
        
        self.cursor.execute(query)
        instrument_ids = []
        for row in self.cursor:
            instrument_ids.append(row[0])
        
        # Function to return quantities of buy and sell for particular instrument
        for instrument_id in instrument_ids:
            query = "SELECT buy_sell, SUM(quantity) FROM Deals WHERE instrument_id = %s GROUP BY instrument_id, buy_sell;"
            self.cursor.execute(query, (instrument_id,))
            buy_sell_quantities = self.cursor.fetchall()

            query = "SELECT price FROM Deals WHERE instrument_id = %s order by deal_time desc limit 1;"
            self.cursor.execute(query, (instrument_id,))
            current_price = float(self.cursor.fetchone()[0])
            buy_quantity = int(buy_sell_quantities[0][1])
            sell_quantity = int(buy_sell_quantities[1][1])

            # Deals with long position (stuff in inventory)
            if buy_quantity >= sell_quantity:
                inventory_leftover_quantity = buy_quantity-sell_quantity
                # 1) Price times quantity for stuff in inventory
                query = """SELECT price, quantity FROM Deals WHERE instrument_id = %s
                 AND buy_sell='B' ORDER BY deal_time desc;"""
                self.cursor.execute(query, (instrument_id,))
                count_quantity = 0
                unrealised_revenue = 0
                for price, quantity in self.cursor:
                    count_quantity+=quantity
                    if count_quantity < inventory_leftover_quantity:
                        unrealised_revenue += quantity * price
                    elif count_quantity == inventory_leftover_quantity:
                        unrealised_revenue += quantity * price
                        break
                    else:
                        count_quantity -= quantity
                        leftover_quantity = inventory_leftover_quantity - count_quantity
                        unrealised_revenue +=  leftover_quantity * price
                        break

                # 2) Current price times quantity
                current_value_of_inventory = inventory_leftover_quantity * current_price
                # 2) - 1) 
               # print(current_value_of_inventory - unrealised_revenue)
                total_effective_profit += current_value_of_inventory - unrealised_revenue
            

            else:
                inventory_deficit = sell_quantity-buy_quantity
                # 1) Price times quantity for stuff in inventory
                query = """SELECT price, quantity FROM Deals WHERE instrument_id = %s
                AND buy_sell='S' ORDER BY deal_time desc;"""
                self.cursor.execute(query, (instrument_id,))
                count_quantity = 0
                revenue_from_short_positions = 0
                for price, quantity in self.cursor:
                    count_quantity+=quantity
                    if count_quantity < inventory_deficit:
                        revenue_from_short_positions += quantity * price
                    elif count_quantity == inventory_deficit:
                        revenue_from_short_positions += quantity * price
                        break
                    else:
                        count_quantity -= quantity
                        leftover_quantity = inventory_deficit - count_quantity
                        revenue_from_short_positions +=  leftover_quantity * price
                        break

                # 2) Current price times quantity
                current_value_of_shorted_stocks = inventory_deficit * current_price
                # 2) - 1) 
               # print(revenue_from_short_positions - current_value_of_shorted_stocks)
                total_effective_profit += revenue_from_short_positions - current_value_of_shorted_stocks
        return total_effective_profit
        
    
    def price_time_graph_of_instrument(self):
        query = """SELECT instrument_name, price, deal_time FROM Deals INNER JOIN Instruments 
        ON Deals.instrument_id=Instruments.instrument_id order by instrument_name, deal_time;"""
        self.open_db_connection()
        self.cursor.execute(query)
        row_headers=[x[0] for x in self.cursor.description]
        records = self.cursor.fetchall()
        self.close_db_connection()
        return(row_headers, records)
    
    def ending_position(self):
        query = "SELECT SUM(price*quantity) FROM Deals GROUP BY buy_sell;"
        self.open_db_connection()
        self.cursor.execute(query)
        row_headers=[x[0] for x in self.cursor.description]
        records = self.cursor.fetchall()
        self.close_db_connection()
        return(row_headers, records)
    

    def instruments_average_price(self):
        query = """SELECT instrument_name, AVG(price) FROM Deals INNER JOIN Instruments ON 
                Deals.instrument_id=Instruments.instrument_id GROUP BY Deals.instrument_id;"""
        self.open_db_connection()
        self.cursor.execute(query)
        row_headers=[x[0] for x in self.cursor.description]
        records = self.cursor.fetchall()
        self.close_db_connection()
        return (row_headers, records)

    def close_db_connection(self):
        if(self.mydb.is_connected()):
            self.cursor.close()
            self.mydb.close()
            print("MySQL connection is closed")
        return
