from flask import Response, Flask 
import time
#import urllib
from mysql.connector import Error
import requests
import json 
#import sys
import db as db 
#sys.path.insert(1,"../datagen")
from datetime import datetime
from flask_restful import Resource, Api, reqparse

app = Flask(__name__)
api = Api(app)


@app.route("/stream")
def stream():
    database = db.db()
    database.open_db_connection()    
    
    link = "http://datagen-casestudygroup23.apps.dbgrads-6eec.openshiftworkshop.com/"
    r = requests.get(link, stream=True)
    def event_stream():    
        for my_json in r.iter_lines():
            if my_json:
                database.insert_deals(json.loads(my_json))
                yield my_json.decode("ascii") + "\n" 
        database.close_db_connection()
    return Response(event_stream(), status=200, mimetype="text/event-stream")

@app.route("/login", methods=["POST"])
def login():     
    parser = reqparse.RequestParser()
    parser.add_argument('username', help = 'This field cannot be blank', required = True)
    parser.add_argument('password', help = 'This field cannot be blank', required = True)
    data = parser.parse_args()
    print(data)

    # making database connection
    database = db.db()
    # database.open_db_connection() 
    record = database.login(data)
    # database.close_db_connection()
    if not record:
        return Response(json.dumps(dict(state='wrong password and username')),  status = 200, mimetype="application/json")
    elif str(record[0]) != data.password:
        return Response(json.dumps(dict(state='wrong password and username')),  status = 200, mimetype="application/json")
    return Response(json.dumps(dict(state='correct')), status = 200, mimetype="application/json")

@app.route("/getallaverages")
def instruments_average_price():
    database = db.db()
    row_headers, records = database.instruments_average_price()
    json_data = []
    for record in records:
        json_data.append(dict(zip(row_headers,record)))
    #print(records)
    return json.dumps(json_data, indent=4)

@app.route("/getalldeals")
def retrieve_all_deals():
    database = db.db()
    row_headers, records = database.retrieve_all_deals()
    json_data = []
    
    for record in records:
        converted_data = []
        for i in range(len(record)):
            if i is not len(record)-1:
                converted_data.append(record[i])
            else:
                converted_data.append(str(record[i]))
        
        json_data.append(dict(zip(row_headers,converted_data)))
    #print(records)
    return json.dumps(json_data, indent=4)

@app.route("/pricevstime")
def price_time_graph_of_instrument():
    database = db.db()
    row_headers, records = database.price_time_graph_of_instrument()
    json_data = []
    for record in records:
        converted_data = []
        for i in range(len(record)):
            if i is not len(record)-1:
                converted_data.append(record[i])
            else:
                converted_data.append(str(record[i]))        
        json_data.append(dict(zip(row_headers,converted_data)))
    #print(records)
    return json.dumps(json_data, indent=4)

@app.route("/endingposition")
def ending_position():
    database = db.db()
    row_headers, records = database.ending_position()
    json_data = []
    sell_minus_buy = list(records[1])[0] - list(records[0])[0]
    json_data.append({"endingposition":sell_minus_buy})    
    return json.dumps(json_data, indent=4)

@app.route("/realisedprofit")
def realised_profit():
    database = db.db()
    realised_profit = database.realised_profit()
    json_data = []
    json_data.append({"realisedprofit":realised_profit})    
    return json.dumps(json_data, indent=4)

@app.route("/effectiveprofit")
def effective_profit():
    database = db.db()
    effective_profit = database.effective_profit()
    json_data = []
    json_data.append({"effectiveprofit":effective_profit})    
    return json.dumps(json_data, indent=4)

def bootapp():
    app.run(port=8080, debug=True, threaded=True, host=("0.0.0.0")) 
    

